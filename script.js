const markAll = document.getElementById("mark-all");
const unreadNotifications = document.querySelectorAll(".unread");
const badge = document.getElementsByClassName("badge");

markAll.addEventListener("click", () => {
  unreadNotifications.forEach((unreadNotification) => {
    unreadNotification.classList.remove("unread");
  });
  badge[0].innerText = 0;
});
